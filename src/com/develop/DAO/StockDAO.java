package com.develop.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;


import com.develo.model.StockM;
import com.develop.config.ConexionDB;
import com.develop.interfaces.StockI;

public class StockDAO implements StockI {

	
	ConexionDB con = new ConexionDB();
	Connection consql;
	PreparedStatement ps;
	ResultSet rs;
	StockM stock = new StockM();
	
	@Override
	public String agregarStock( String codigo, String descripcion, String categoria,String tamanio) {
		

		
		String sql = "INSERT INTO `stock` "+"( `codigo`,`descripcion`,`categoria`,`tamanio`) VALUES "+"( '" + codigo +"','" + descripcion + "','"+ categoria +"','" + tamanio +"')";
    	String respuesta = "";
    	
   
    	
    	respuesta = "ok";
    	try {
    		
    		consql = con.getConnection();
    		ps= consql.prepareStatement(sql);
    		ps.execute();
    		
    	 	System.out.println("se inserto correctamente");
    	}catch (Exception e) {
    		System.out.println(e);
    	}
		return respuesta;
		
	}

	@Override
	public ArrayList<StockM> listarStock() {
		String  nuevoSql = "SELECT `codigo`,`descripcion`,`categoria`,`tamanio` FROM `stock`;";
    	String resp= "";
    	
    	ArrayList<StockM> listaU = new ArrayList<StockM>();
    	try {
    		
    		consql = con.getConnection();
    		ps = consql.prepareStatement(nuevoSql);
    		rs = ps.executeQuery();
    		
    		while(rs.next()) {
    			
    			
    			StockM  stock= new StockM();
    			stock.setCodigo(rs.getString("codigo"));
    			stock.setDescripcion(rs.getString("descripcion"));
    			stock.setCategoria(rs.getString("categoria"));
    			stock.setTamanio(rs.getString("tamanio"));
    			
                
                listaU.add(stock);
                System.out.println(nuevoSql);
                
    		}
    			
    		resp= "ok";
    	}catch(Exception e){
    		System.out.println("Error de conexion");
    		
    		System.out.println("Error" + e);
    	}
    	return listaU;
	}
}
