package com.develop.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

import com.develo.model.VentaM;
import com.develop.config.ConexionDB;
import com.develop.interfaces.VentasI;

public class VentasDAO implements VentasI{
	ConexionDB con = new ConexionDB();
	Connection consql;
	PreparedStatement ps;
	ResultSet rs;
	VentaM venta = new VentaM();
	
	@Override
	public String agregarVenta( int idVenta, int idCliente) {
		

		LocalDate localDate = LocalDate.now();
		LocalTime localTime = LocalTime.now();
		String sql = "INSERT INTO `venta` "+"( `idVenta`,`idCliente`,`fechaCompra`,`horaCompra`) VALUES "+"( '" + idVenta +"','" + idCliente +"','"+ localDate+"','" +localTime+"')";
    	String respuesta = "";
    	
   
    	
    	respuesta = "ok";
    	try {
    		
    		consql = con.getConnection();
    		ps= consql.prepareStatement(sql);
    		ps.execute();
    		
    	 	System.out.println("se inserto correctamente");
    	}catch (Exception e) {
    		System.out.println(e);
    	}
		return respuesta;
		
	}

	@Override
	public ArrayList<VentaM> listarVentas() {
		String  nuevoSql = "SELECT `idVenta`,`idCliente`,`fechaCompra`,`horaCompra` FROM `venta`;";
    	String resp= "";
    	
    	ArrayList<VentaM> listaU = new ArrayList<VentaM>();
    	try {
    		
    		consql = con.getConnection();
    		ps = consql.prepareStatement(nuevoSql);
    		rs = ps.executeQuery();
    		
    		while(rs.next()) {
    			
    			
    			VentaM  venta= new VentaM();
    			
    			venta.setIdVenta(rs.getInt("idVenta"));
    			venta.setIdCliente(rs.getInt("idCliente"));
    			venta.setFechaCompra(rs.getDate("fechaCompra"));
    			venta.setHoraCompra(rs.getTime("horaCompra"));
                
                listaU.add(venta);
                System.out.println(nuevoSql);
                
    		}
    			
    		resp= "ok";
    	}catch(Exception e){
    		System.out.println("Error de conexion");
    		
    		System.out.println("Error" + e);
    	}
    	return listaU;
	}

	

}
