package com.develop.DAO;

import com.develo.model.ProductoM;
import com.develo.model.UusuarioM;
import com.develop.config.ConexionDB;
import com.develop.interfaces.ProductoI;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;


public class ProductoDAO implements ProductoI {

	
	ConexionDB con = new ConexionDB();
	Connection consql;
	PreparedStatement ps;
	ResultSet rs;
	ProductoM producto = new ProductoM();
	
	@Override
	public String agregarProducto( String codigo, String nombre, String marca, String tamanio, String stock) {
		// TODO Auto-generated method stub
		
		
		LocalDate localDate = LocalDate.now();
		LocalTime localTime = LocalTime.now();
		String sql = "INSERT INTO `producto` "+"(`codigo`, `nombre`, `marca`, `tamanio`, `stock`,`fecha`,`hora`) VALUES "+"('" + codigo +" ', '" + nombre +"','" + marca + "','"+ tamanio +"','" + stock +"','"+ localDate + "','"+ localTime +"')";
    	String respuesta = "";
    	
   
    	
    	respuesta = "ok";
    	try {
    		
    		consql = con.getConnection();
    		ps= consql.prepareStatement(sql);
    		ps.execute();
    		
    	 	System.out.println("se inserto correctamente");
    	}catch (Exception e) {
    		System.out.println(e);
    	}
		return respuesta;
	}
	
	public ArrayList<ProductoM> listarProducto(){
		String  nuevoSql = "SELECT `idProveedor`,`codigo`,`nombre`,`marca`,`tamanio`,`stock`,`fecha`,`hora` FROM `producto`;";
    	String resp= "";
    	
    	ArrayList<ProductoM> listaU = new ArrayList<ProductoM>();
    	try {
    		
    		consql = con.getConnection();
    		ps = consql.prepareStatement(nuevoSql);
    		rs = ps.executeQuery();
    		
    		while(rs.next()) {
    			
    			
    			ProductoM  producto= new ProductoM();
                producto.setIdProveedor(rs.getInt("idProveedor"));
                producto.setCodigo(rs.getString("codigo"));
                producto.setNombre(rs.getString("nombre"));
                producto.setMarca(rs.getString("marca"));
                producto.setTamanio(rs.getString("tamanio"));
                producto.setStock(rs.getString("stock"));
                producto.setFecha(rs.getDate("fecha"));
                producto.setHora(rs.getTime("hora"));
                
                listaU.add(producto);
                System.out.println(nuevoSql);
                
    		}
    			
    		resp= "ok";
    	}catch(Exception e){
    		System.out.println("Error de conexion");
    		
    		System.out.println("Error" + e);
    	}
    	return listaU;
		
	}
	
	


	}
	
	


