package com.develop.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;


import com.develo.model.ProveedoresM;
import com.develop.config.ConexionDB;
import com.develop.interfaces.ProveedorI;

public class ProveedorDAO implements ProveedorI{

	ConexionDB con = new ConexionDB();
	Connection consql;
	PreparedStatement ps;
	ResultSet rs;
	ProveedoresM proveedor = new ProveedoresM();
	
	@Override
	public String AgregarProveedor( String nombre, String direccion, String correo,String telefono) {
		

		LocalDate localDate = LocalDate.now();
		LocalTime localTime = LocalTime.now();
		String sql = "INSERT INTO `producto` "+"( `nombre`, `direccion`, `correo`, `telefono`,`fechaAltaProveedor`,`horaAltaProveedor`) VALUES "+"( '" + nombre +"','" + direccion + "','"+ correo +"','" + telefono +"','"+ localDate + "','"+ localTime +"')";
    	String respuesta = "";
    	
   
    	
    	respuesta = "ok";
    	try {
    		
    		consql = con.getConnection();
    		ps= consql.prepareStatement(sql);
    		ps.execute();
    		
    	 	System.out.println("se inserto correctamente");
    	}catch (Exception e) {
    		System.out.println(e);
    	}
		return respuesta;
		
	}

	@Override
	public ArrayList<ProveedoresM> listarProveedor() {
		String  nuevoSql = "SELECT `nombre`,`direccion`,`correo`,`telefono`,`fechaAltaProveedor`,`horaAltaProveedor` FROM `proveedores`;";
    	String resp= "";
    	
    	ArrayList<ProveedoresM> listaU = new ArrayList<ProveedoresM>();
    	try {
    		
    		consql = con.getConnection();
    		ps = consql.prepareStatement(nuevoSql);
    		rs = ps.executeQuery();
    		
    		while(rs.next()) {
    			
    			
    			ProveedoresM  proveedor= new ProveedoresM();
    			proveedor.setIdProveedor(rs.getInt("idProveedores"));
    			proveedor.setNombre(rs.getString("nombre"));
    			proveedor.setDireccion(rs.getString("direccion"));
    			proveedor.setCorreo(rs.getString("correo"));
    			proveedor.setTelefono(rs.getString("Telefono"));
    			proveedor.setFechaAltaProveedor(rs.getDate("fechaAltaProveedor"));
    			proveedor.setHoraAltaProveedor(rs.getTime("horaAltaProveedor"));
                
                listaU.add(proveedor);
                System.out.println(nuevoSql);
                
    		}
    			
    		resp= "ok";
    	}catch(Exception e){
    		System.out.println("Error de conexion");
    		
    		System.out.println("Error" + e);
    	}
    	return listaU;
	}
}
