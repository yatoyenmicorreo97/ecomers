package com.develop.controller.producto;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.develop.DAO.ProductoDAO;
import com.develop.DAO.UsuarioDAO;

/**
 * Servlet implementation class ProductoC
 */
@WebServlet("/ProductoC")
public class ProductoC extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ProductoC() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
		
		RequestDispatcher rq;
		String codigo = request.getParameter("codigo");
		String nombre = request.getParameter("nombre");
		String marca = request.getParameter("marca");
		String tamanio = request.getParameter("tamanio");
		String stock = request.getParameter("stock");
		
		System.out.println(codigo +"  "+ nombre+ " " +marca+ " " + tamanio + " " + stock);
		ProductoDAO dao = new ProductoDAO(); 
		
		String respuesta = dao.agregarProducto(codigo,nombre,marca,tamanio,stock);
		
		
		
	}

}
