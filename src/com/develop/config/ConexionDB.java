package com.develop.config;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;



public class ConexionDB {
	


	    public static final String URL = "jdbc:mysql://localhost:3306/db_eccomers";
	    public static final String USER = "root2";
	    public static final String CLAVE = "12131415a";
	    Connection con;

	    public ConexionDB() {
	        try {
	            Class.forName("com.mysql.cj.jdbc.Driver");
	            con = (Connection) DriverManager.getConnection(URL, USER, CLAVE);
	        } catch (Exception e) {
	            System.out.println("Error " + e.getMessage());
	        }

	    }
	    public Connection getConnection() {
	    	return con;
}
	    }
