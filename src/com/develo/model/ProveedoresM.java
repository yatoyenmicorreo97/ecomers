package com.develo.model;

import java.sql.Time;
import java.util.Date;

public class ProveedoresM {
	
	
	private int idProveedor;
	private String nombre;
	private String direccion;
	private String correo;
	private String telefono;
	private Date fechaAltaProveedor;
	private Time horaAltaProveedor;
	public ProveedoresM() {
		super();
	}
	public ProveedoresM(int idProveedor, String nombre, String direccion, String correo, String telefono,
			Date fechaAltaProveedor, Time horaAltaProveedor) {
		super();
		this.idProveedor = idProveedor;
		this.nombre = nombre;
		this.direccion = direccion;
		this.correo = correo;
		this.telefono = telefono;
		this.fechaAltaProveedor = fechaAltaProveedor;
		this.horaAltaProveedor = horaAltaProveedor;
	}
	public int getIdProveedor() {
		return idProveedor;
	}
	public void setIdProveedor(int idProveedor) {
		this.idProveedor = idProveedor;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public Date getFechaAltaProveedor() {
		return fechaAltaProveedor;
	}
	public void setFechaAltaProveedor(Date fechaAltaProveedor) {
		this.fechaAltaProveedor = fechaAltaProveedor;
	}
	public Time getHoraAltaProveedor() {
		return horaAltaProveedor;
	}
	public void setHoraAltaProveedor(Time horaAltaProveedor) {
		this.horaAltaProveedor = horaAltaProveedor;
	}	

}
