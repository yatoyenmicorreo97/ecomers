package com.develo.model;
import java.util.*;
import java.sql.Time;


public class VentaM {
	public int idVenta;
	public int idCliente;
	public Date fechaCompra;
	public Time horaCompra;
	
	
	public VentaM() {
		super();
	}


	public VentaM(int idVenta, int idCliente, Date fechaCompra, Time horaCompra) {
		super();
		this.idVenta = idVenta;
		this.idCliente = idCliente;
		this.fechaCompra = fechaCompra;
		this.horaCompra = horaCompra;
	}


	public int getIdVenta() {
		return idVenta;
	}


	public void setIdVenta(int idVenta) {
		this.idVenta = idVenta;
	}


	public int getIdCliente() {
		return idCliente;
	}


	public void setIdCliente(int idCliente) {
		this.idCliente = idCliente;
	}


	public Date getFechaCompra() {
		return fechaCompra;
	}


	public void setFechaCompra(Date fechaCompra) {
		this.fechaCompra = fechaCompra;
	}


	public Time getHoraCompra() {
		return horaCompra;
	}


	public void setHoraCompra(Time horaCompra) {
		this.horaCompra = horaCompra;
	}
	
	
	
	
}