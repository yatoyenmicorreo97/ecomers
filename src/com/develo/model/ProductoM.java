package com.develo.model;

import java.sql.Time;
import java.util.Date;


public class ProductoM {
	
	private int idProducto;
	private int idProveedor;
	private String codigo;
	private String nombre;
	private String marca;
	private String tamanio;
	private String stock;
	private Date fecha;
	private Time hora;
	
	
	public ProductoM(){
		
	}

	public ProductoM( String codigo, String nombre, String marca, String tamanio,String stock,Date fecha,Time hora) {
		super();
		this.idProducto = idProducto;
		this.idProveedor = idProveedor;
		this.codigo = codigo;
		this.nombre = nombre;
		this.marca = marca;
		this.tamanio = tamanio;
		this.stock = stock;
		this.fecha = fecha;
		this.hora = hora;
	}

	public int getIdProducto() {
		return idProducto;
	}

	public void setIdProducto(int idProducto) {
		this.idProducto = idProducto;
	}

	public int getIdProveedor() {
		return idProveedor;
	}

	public void setIdProveedor(int idProveedor) {
		this.idProveedor = idProveedor;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getTamanio() {
		return tamanio;
	}

	public void setTamanio(String tamanio) {
		this.tamanio = tamanio;
	}

	public String getStock() {
		return stock;
	}

	public void setStock(String stock) {
		this.stock = stock;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Time getHora() {
		return hora;
	}

	public void setHora(Time hora) {
		this.hora = hora;
	}
	
	
	
	
	
 

}
