package com.develo.model;

import java.sql.Time;
import java.util.Date;

public class UusuarioM {

	private int idUsuario;
	private String nombre;
	private String password;
	private String usuario;
	private Date fecha;
	private Time hora;
	
	
	
	public UusuarioM() {
		super();
	}
	public UusuarioM(int idUsuario, String nombre, String password, String usuario, Date fecha, Time hora) {
		super();
		this.idUsuario = idUsuario;
		this.nombre = nombre;
		this.password = password;
		this.usuario = usuario;
		this.fecha = fecha;
		this.hora = hora;
		
	
	
		
	}
	public int getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public Time getHora() {
		return hora;
	}
	public void setHora(Time hora) {
		this.hora = hora;
	}
	
	
	
}
