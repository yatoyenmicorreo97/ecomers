package com.develo.model;

public class StockM {
	
	private String codigo;
	private int idProducto;
	private int idProveedor;
	private String descripcion;
	private String categoria;
	private String tamanio;
	
	
	
	public StockM() {
		super();
	}

	public StockM(String codigo, int idProducto, int idProveedor, String descripcion, String categoria, String tamanio) {
		super();
		this.codigo = codigo;
		this.idProducto = idProducto;
		this.idProveedor = idProveedor;
		this.descripcion = descripcion;
		this.categoria = categoria;
		this.tamanio = tamanio;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public int getIdProducto() {
		return idProducto;
	}

	public void setIdProducto(int idProducto) {
		this.idProducto = idProducto;
	}

	public int getIdProveedor() {
		return idProveedor;
	}

	public void setIdProveedor(int idProveedor) {
		this.idProveedor = idProveedor;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public String getTamanio() {
		return tamanio;
	}

	public void setTamanio(String tamanio) {
		this.tamanio = tamanio;
	}
		

}